#if !defined(AFX_ACTIVE_PITCHCTL_H__E08D7B7E_2520_43A3_AB01_E4CFF65C418F__INCLUDED_)
#define AFX_ACTIVE_PITCHCTL_H__E08D7B7E_2520_43A3_AB01_E4CFF65C418F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <math.h>

// Active_pitchCtl.h : Declaration of the CActive_pitchCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl : See Active_pitchCtl.cpp for implementation.

class CActive_pitchCtrl : public COleControl
{
	DECLARE_DYNCREATE(CActive_pitchCtrl)

// Constructor
public:
	CActive_pitchCtrl();
	double pitch_angle;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CActive_pitchCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CActive_pitchCtrl();

	DECLARE_OLECREATE_EX(CActive_pitchCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CActive_pitchCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CActive_pitchCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CActive_pitchCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CActive_pitchCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CActive_pitchCtrl)
	double m_pitchHeight;
	afx_msg void OnPitchHeightChanged();
	double m_pitchWidth;
	afx_msg void OnPitchWidthChanged();
	afx_msg void pitch(double angle);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CActive_pitchCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CActive_pitchCtrl)
	dispidPitchHeight = 1L,
	dispidPitchWidth = 2L,
	dispidPitch = 3L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVE_PITCHCTL_H__E08D7B7E_2520_43A3_AB01_E4CFF65C418F__INCLUDED)
