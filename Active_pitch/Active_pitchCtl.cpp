// Active_pitchCtl.cpp : Implementation of the CActive_pitchCtrl ActiveX Control class.

#include "stdafx.h"
#include "Active_pitch.h"
#include "Active_pitchCtl.h"
#include "Active_pitchPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CActive_pitchCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CActive_pitchCtrl, COleControl)
	//{{AFX_MSG_MAP(CActive_pitchCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CActive_pitchCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CActive_pitchCtrl)
	DISP_PROPERTY_NOTIFY(CActive_pitchCtrl, "pitchHeight", m_pitchHeight, OnPitchHeightChanged, VT_R8)
	DISP_PROPERTY_NOTIFY(CActive_pitchCtrl, "pitchWidth", m_pitchWidth, OnPitchWidthChanged, VT_R8)
	DISP_FUNCTION(CActive_pitchCtrl, "pitch", pitch, VT_EMPTY, VTS_R8)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CActive_pitchCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CActive_pitchCtrl, COleControl)
	//{{AFX_EVENT_MAP(CActive_pitchCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CActive_pitchCtrl, 1)
	PROPPAGEID(CActive_pitchPropPage::guid)
END_PROPPAGEIDS(CActive_pitchCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CActive_pitchCtrl, "ACTIVEPITCH.ActivepitchCtrl.1",
	0xd28fcc1f, 0x68b8, 0x4fd7, 0x8a, 0x77, 0xbb, 0xab, 0x72, 0xd7, 0xdf, 0x2d)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CActive_pitchCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DActive_pitch =
		{ 0xe3126579, 0xbc6d, 0x45fa, { 0xa0, 0x92, 0xf5, 0xa4, 0x42, 0x18, 0x96, 0x7a } };
const IID BASED_CODE IID_DActive_pitchEvents =
		{ 0x5dd708fe, 0x84dd, 0x4549, { 0xb5, 0x75, 0x7c, 0x21, 0x9, 0x1a, 0x52, 0x78 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwActive_pitchOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CActive_pitchCtrl, IDS_ACTIVE_PITCH, _dwActive_pitchOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::CActive_pitchCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CActive_pitchCtrl

BOOL CActive_pitchCtrl::CActive_pitchCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_ACTIVE_PITCH,
			IDB_ACTIVE_PITCH,
			afxRegInsertable | afxRegApartmentThreading,
			_dwActive_pitchOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::CActive_pitchCtrl - Constructor

CActive_pitchCtrl::CActive_pitchCtrl()
{
	InitializeIIDs(&IID_DActive_pitch, &IID_DActive_pitchEvents);

	// TODO: Initialize your control's instance data here.
	pitch_angle=0.0f;
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::~CActive_pitchCtrl - Destructor

CActive_pitchCtrl::~CActive_pitchCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::OnDraw - Drawing function
CPoint rotatePoint(CPoint initial,CPoint center,double seta)
{
	seta=seta*3.1415926/180;
	CPoint result;
	result.x=(initial.x-center.x)*cos(seta)-(initial.y-center.y)*sin(seta)+center.x;
	result.y=(initial.x-center.x)*sin(seta)+(initial.y-center.y)*cos(seta)+center.y;
	return result;
}
void CActive_pitchCtrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.	
	double m_height=m_pitchHeight,m_width=m_pitchWidth;
	//画背景
	CPen mypen0;
	CBrush myBrush0;
	mypen0.CreatePen(PS_SOLID,1,RGB(0,0,205));
	myBrush0.CreateSolidBrush(RGB(0,0,205));
	pdc->SelectObject(mypen0);
	pdc->SelectObject(myBrush0);
	pdc->Rectangle(0,0,m_width,m_height);

	double x0=m_width/2,y0=m_height/2;

	CPen mypen;
	CBrush myBrush;
	mypen.CreatePen(PS_SOLID,1,RGB(255,165,0));
	myBrush.CreateSolidBrush(RGB(255,165,0));
	pdc->SelectObject(mypen);
	pdc->SelectObject(myBrush);
	//画圆
	double seta=pitch_angle;
	double r=m_height/8;    //半径
	double cir_x0=m_width-10-r,cir_y0=y0;
	CPoint cir0(cir_x0,cir_y0);                   //初始圆心坐标
	CPoint center(x0,y0);                         //旋转中心坐标
	CPoint cir=rotatePoint(cir0,center,seta);     //获取旋转seta角度后的圆心坐标
	pdc->Ellipse(cir.x-r,cir.y-r,cir.x+r,cir.y+r);//画旋转后的圆
	
	/*----------------------------画艉部-------------------------*/
	//初始点p1
	double y1=y0-r-5,x1=10;    
	CPoint initial_x1(x1,y1);
	CPoint rotate_x1=rotatePoint(initial_x1,center,seta);   //旋转后的坐标
	pdc->BeginPath();
	pdc->MoveTo(rotate_x1.x,rotate_x1.y);
	//p2
	double x2=10+m_width/18,y2=y0-r-5;
	CPoint initial_x2(x2,y2);
	CPoint rotate_x2=rotatePoint(initial_x2,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x2.x,rotate_x2.y);                   //p1-->p2
	
	//p3
	double x3=x2+(r+5)*tan(10*3.1415926/180),y3=y0-3;
	CPoint initial_x3(x3,y3);
	CPoint rotate_x3=rotatePoint(initial_x3,center,seta);   //旋转后的坐标

	pdc->LineTo(rotate_x3.x,rotate_x3.y);                   //p2-->p3
	
	//p4
	double x4=x3+r/tan(30*3.1415926/180),y4=y0-r;
	CPoint initial_x4(x4,y4);
	CPoint rotate_x4=rotatePoint(initial_x4,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x4.x,rotate_x4.y);                   //p3-->p4
	
	//p5
	double x5=x4,y5=y0+r;
	CPoint initial_x5(x5,y5);
	CPoint rotate_x5=rotatePoint(initial_x5,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x5.x,rotate_x5.y);                   //p4-->p5

	//p6
	double x6=x3,y6=y0+3;
	CPoint initial_x6(x6,y6);
	CPoint rotate_x6=rotatePoint(initial_x6,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x6.x,rotate_x6.y);                   //p5-->p6
	
	//p7
	double x7=x2,y7=y0+r+5;
	CPoint initial_x7(x7,y7);
	CPoint rotate_x7=rotatePoint(initial_x7,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x7.x,rotate_x7.y);                   //p6-->p7
	
	//p8
	double x8=10,y8=y0+r+5;
	CPoint initial_x8(x8,y8);
	CPoint rotate_x8=rotatePoint(initial_x8,center,seta);   //旋转后的坐标
	
	pdc->LineTo(rotate_x8.x,rotate_x8.y);                   //p7-->p8

	pdc->LineTo(rotate_x1.x,rotate_x1.y);                   //p8-->p1
	pdc->EndPath();
	CRgn rgn;
	rgn.CreateFromPath(pdc);
	pdc->InvertRgn(&rgn);
	pdc->FillRgn(&rgn,&myBrush);

	/*-----------------------------画矩形------------------------------------*/
	//右下角坐标
	double x9=m_width-10-r,y9=y0+r;                        
	CPoint initial_x9(x9,y9);
	CPoint rotate_x9=rotatePoint(initial_x9,center,seta);        //旋转后的坐标
	//右上角坐标
	double x10=m_width-10-r,y10=y0-r;                        
	CPoint initial_x10(x10,y10);
	CPoint rotate_x10=rotatePoint(initial_x10,center,seta);        //旋转后的坐标
	//左上角坐标等于p4,左下角坐标等于p5
	pdc->BeginPath();
	pdc->MoveTo(rotate_x4.x,rotate_x4.y);
	pdc->LineTo(rotate_x10.x,rotate_x10.y);
	pdc->LineTo(rotate_x9.x,rotate_x9.y);
	pdc->LineTo(rotate_x5.x,rotate_x5.y);
	pdc->LineTo(rotate_x4.x,rotate_x4.y);
	pdc->EndPath();

	CRgn rgn1;
	rgn1.CreateFromPath(pdc);
	pdc->InvertRgn(&rgn1);
	pdc->FillRgn(&rgn1,&myBrush);

	/*---------------------------------画竖线---------------------------------*/
	double x11=m_width*3/4,y11=y0;     //起点
	CPoint initial_x11(x11,y11);
	CPoint rotate_x11=rotatePoint(initial_x11,center,seta);        //旋转后的坐标
	double x12=m_width*3/4,y12=y0-2*r;
	CPoint initial_x12(x12,y12);
	CPoint rotate_x12=rotatePoint(initial_x12,center,seta);        //旋转后的坐标

	CPen mypen1;
	//CBrush myBrush;
	mypen1.CreatePen(PS_SOLID,16,RGB(255,165,0));
//	myBrush.CreateSolidBrush(RGB(255,165,0));
	pdc->SelectObject(mypen1);
	pdc->MoveTo(rotate_x11.x,rotate_x11.y);
	pdc->LineTo(rotate_x12.x,rotate_x12.y);

	//画字符“Pitch”
	pdc->SetTextColor(RGB(255,165,0));
	pdc->SetBkColor(RGB(0,0,205));
	pdc->TextOut(x0-13,y0+r+15,_T("Pitch"));
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::DoPropExchange - Persistence support

void CActive_pitchCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
	PX_Double(pPX,_T("pitchWidth"),m_pitchWidth,300);
	PX_Double(pPX,_T("pitchHeight"),m_pitchHeight,120);

}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::OnResetState - Reset control to default state

void CActive_pitchCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl::AboutBox - Display an "About" box to the user

void CActive_pitchCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_ACTIVE_PITCH);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CActive_pitchCtrl message handlers

void CActive_pitchCtrl::pitch(double angle) 
{
	// TODO: Add your dispatch handler code here
	pitch_angle=angle;
}

void CActive_pitchCtrl::OnPitchHeightChanged() 
{
	// TODO: Add notification handler code
	InvalidateControl();
	SetModifiedFlag();
}

void CActive_pitchCtrl::OnPitchWidthChanged() 
{
	// TODO: Add notification handler code
	InvalidateControl();
	SetModifiedFlag();
}
