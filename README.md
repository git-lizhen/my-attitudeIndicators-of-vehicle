# my attitudeIndicators of vehicle

项目在VS2005中可正常运行。

利用MFC绘制水下机器人的姿态显示仪(ActiveX,可直接嵌入到其他MFC等界面工程中)，可实时显示水下机器人的偏航角、俯仰角以及纵倾角。

程序中还有三个项目：
（1）Active_heading:用于显示偏航角角的姿态仪
（2）Active_pitch:用于显示纵倾角的姿态仪
（3）Active_roll:用于显示横滚角的姿态仪

最终生成的.ocx姿态仪界面如下所示：

![输入图片说明](https://gitee.com/uploads/images/2018/0329/230228_ec02fd96_1477507.png "绘图3.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0329/230240_47e5051b_1477507.png "绘图2.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0329/230249_fa6e0ce4_1477507.png "绘图1.png")

每个姿态仪都有一个角度属性，用以控制姿态仪的偏转角度。

具体的工程建立过程以及.ocx文件的注册及使用，可参考本人的博客（地址：https://blog.csdn.net/weixin_38215395/article/details/79718353）。


/*--------------------------------------ActiveX控件的注册及使用---------------------------------------*/
1、ocx控件的注册

ocx的注册，网上有好多文章有写，这里采用一种比较笨的方法，在cmd中注册。其他方法可百度。首先，以管理员身份运行cmd，从cmd中进入到项目的Debug文件夹，Debug文件夹中有项目生成的.ocx文件，如“1111.ocx”。在cmd中输入regsvr32 xxxx.ocx。显示注册成功即可。

2、ocx控件的使用

新建MFC工程，在Dialog中找到设计界面。在界面空白处，右击，选择“插入Active控件…”，选择刚才注册的控件。你自己设计的控件就显示出来了^-^。在VC++6.0中，右击属性，会显示之前设置的属性，改变属性值，控件也会发生相应的变化。在本例中，可改变控件的宽和高。 
那么，空间中，矩形的旋转角度该如何改变呢？ 
右击控件，选择“建立类向导”（更高版本中，为“添加变量”）->”Member Variabvles”。双击控价名称，会提示建立“控件对应的类”。点击确定后，控件的接口类就会自动添加到程序中。之后显示“Add Member Variable”对话框，如下： 

![输入图片说明](https://gitee.com/uploads/images/2018/0330/090331_b1c4a67b_1477507.png "添加变量.png")

上图中显示了添加的变量名称（对应类的一个对象），类型为“control”表明可实现控件的方法。 
点击确定后，在界面类的头文件“xxxxDlg.h”中，就会出现控件类的angle的对象。利用这个对象就可以设置控件中的旋转角度。

联系：李震 1039953685@qq.com
若使用请注明出处： lizhen@copyright.


