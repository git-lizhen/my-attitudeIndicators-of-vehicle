// Active_headingPpg.cpp : Implementation of the CActive_headingPropPage property page class.

#include "stdafx.h"
#include "Active_heading.h"
#include "Active_headingPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CActive_headingPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CActive_headingPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CActive_headingPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CActive_headingPropPage, "ACTIVEHEADING.ActiveheadingPropPage.1",
	0xbffa4774, 0x61a2, 0x40fa, 0x87, 0xf6, 0xb6, 0x32, 0x59, 0x1d, 0x36, 0x5f)


/////////////////////////////////////////////////////////////////////////////
// CActive_headingPropPage::CActive_headingPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CActive_headingPropPage

BOOL CActive_headingPropPage::CActive_headingPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_ACTIVE_HEADING_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingPropPage::CActive_headingPropPage - Constructor

CActive_headingPropPage::CActive_headingPropPage() :
	COlePropertyPage(IDD, IDS_ACTIVE_HEADING_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CActive_headingPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingPropPage::DoDataExchange - Moves data between page and properties

void CActive_headingPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CActive_headingPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CActive_headingPropPage message handlers
