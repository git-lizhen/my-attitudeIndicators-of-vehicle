#if !defined(AFX_ACTIVE_HEADINGCTL_H__6D919EEE_C28B_4953_8C6C_8A24E2B372BC__INCLUDED_)
#define AFX_ACTIVE_HEADINGCTL_H__6D919EEE_C28B_4953_8C6C_8A24E2B372BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Active_headingCtl.h : Declaration of the CActive_headingCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CActive_headingCtrl : See Active_headingCtl.cpp for implementation.
#include<math.h>
class CActive_headingCtrl : public COleControl
{
	DECLARE_DYNCREATE(CActive_headingCtrl)

// Constructor
public:
	CActive_headingCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CActive_headingCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CActive_headingCtrl();

	DECLARE_OLECREATE_EX(CActive_headingCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CActive_headingCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CActive_headingCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CActive_headingCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CActive_headingCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CActive_headingCtrl)
	double m_size;
	afx_msg void OnSizeChanged();
	afx_msg void HeadingAngle(double angle);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CActive_headingCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CActive_headingCtrl)
	dispidSize = 1L,
	dispidHeadingAngle = 2L,
	//}}AFX_DISP_ID
	};
private:
	double heading_angle;              //����Ǵ�С
	void DrawArrow(CDC *dc,CPoint p1,CPoint p2,double theta,int length);

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ACTIVE_HEADINGCTL_H__6D919EEE_C28B_4953_8C6C_8A24E2B372BC__INCLUDED)
